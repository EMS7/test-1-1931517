package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplication() {
		int[][] testMatrix = {{ 1,2,3} , {4,5,6 }};
		int[][] expectedMatrix = {{ 1,2,3,1,2,3} , {4,5,6,4,5,6}};
		assertArrayEquals(expectedMatrix, MatrixMethod.duplicate(testMatrix) );
		int[][] testMatrix_2 = {{ 1,2,3,4} , {5,6,7,8 }};
		int[][] expectedMatrix_2 = {{ 1,2,3,4,1,2,3,4} , {5,6,7,8,5,6,7,8}};
		assertArrayEquals(expectedMatrix_2, MatrixMethod.duplicate(testMatrix_2) );
	}

}
