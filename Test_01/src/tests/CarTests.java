package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import vehicles.Car;

class CarTests {

	@Test
	//test getSpeed
	public void testGetSpeed() {
		Car c1 = new Car(5);
		assertEquals(5, c1.getSpeed());
		
	}
	@Test
	public void testLocation() {
		Car c1 = new Car(10);
		//moving right twice
		c1.moveRight();
		c1.moveRight();
		assertEquals(20, c1.getLocation());
		c1.moveLeft();
		assertEquals(10, c1.getLocation());
		
	}
	
	@Test
	public void testAcceleration() {
	Car c1 = new Car(5);
	//expecting +3 in speed
	c1.accelerate();
	c1.accelerate();
	c1.accelerate();
	assertEquals(8, c1.getSpeed());
	c1.decelerate();
	c1.decelerate();
	//expecting -2 in speed
	assertEquals(6, c1.getSpeed());
	
	}
	
	@Test
	public void testInitialCar() {
		Car c1 = new Car(8);
		assertEquals(0, c1.getLocation());
		assertEquals(8, c1.getSpeed());
	}
	
	@Test
	public void TestDeccelerationAfterZero() {
		Car c1 = new Car(2);
		//2-4 should still be 0 even if it gives us -2
		c1.decelerate();
		c1.decelerate();
		c1.decelerate();
		c1.decelerate();
		assertEquals(0, c1.getSpeed());
	}

}
