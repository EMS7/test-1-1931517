package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] m_arr){
		final int duplicate = 2;
		int[][] temp = new int[m_arr.length][m_arr[0].length*duplicate];
		//put original content in new array
		for(int i = 0; i < m_arr.length;i++) {
			for(int j = 0; j < m_arr[0].length; j++) {
				temp[i][j] = m_arr[i][j];
			}
		}
		//duplicate the original content
		for(int i = 0; i < m_arr.length; i++) {
			for(int j = 0; j < m_arr[0].length; j++) {
				temp[i][m_arr[0].length + j] = m_arr[i][j];
			}
		}
		return temp;
	}
	

}
